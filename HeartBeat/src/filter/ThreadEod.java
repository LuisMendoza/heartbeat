package filter;

import java.math.BigDecimal;

import javax.swing.plaf.SliderUI;

public class ThreadEod implements Runnable {

	private int start;
	private int end;
	private BigDecimal[][] matrix;
	private BigDecimal[][] zeros;
	
	
	public ThreadEod(int start, int end, BigDecimal[][] matrix,
			BigDecimal[][] zeros) {
		super();
		this.start = start;
		this.end = end;
		this.matrix = matrix;
		this.zeros = zeros;
	}


	@Override
	public void run() {
		for (int w = start; w < end; w++) {
			for (int j = 0; j < matrix[w].length; j++) {
				zeros[w][j] = matrix[w][j];
			}
		}

	}

}
