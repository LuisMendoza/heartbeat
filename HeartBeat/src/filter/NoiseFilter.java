package filter;

//import android.util.Log;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;


public class NoiseFilter {
    /**
     * Se emplea la escala 2^4 para detectar los picos R, 150 00 x 1
     */
	private static final SimpleDateFormat sdf =  new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSS'Z'");
    private static final int _SCALE = 4;
    private static final RoundingMode ROUNDING_MODE = RoundingMode.HALF_DOWN;
    private static final int DECIMAL_PRECISION = 8;
	private static final String SEPARATOR_FILE = ",";
   /* private static BigDecimal[][] sample = {{point("8.310376391601563E-4"),
            point("9.958325610351561E-4"), point("1.6479492187499985E-4"),
            point("9.134351000976563E-4"), point("3.331213586425782E-4"),
            point("5.803137414550779E-4"), point("8.310376391601563E-4"),
            point("9.843884692382811E-4"), point("8.310376391601563E-4"),
            point("9.843884692382811E-4")}};*/
    private static BigDecimal[][] scales = {{point(1), point(2), point(4),
            point(8)}};

    //frecuencia
    private static BigDecimal sfreq = point(250);//dato entrada
    private static Integer SAMPLES2READ = 300;//dato entrada

    // outs
    private static BigDecimal[][] filsig1;
    private static BigDecimal[][] filsig2;
    private static BigDecimal[][] filsig3;
    private static BigDecimal[][] tn;
    private static BigDecimal[][] D_ACOND;
    private static BigDecimal[][] x;

    private static BigDecimal[][] R_amp_ECG;
    private static BigDecimal[][] R_amp;
    private static BigDecimal[][] R_t;
    private static BigDecimal[][] R_index;

    private static BigDecimal[][] S_amp_ECG;
    private static BigDecimal[][] S_amp;
    private static BigDecimal[][] S_t;
    private static BigDecimal[][] S_index;

    private static BigDecimal[][] Q_amp_ECG;
    private static BigDecimal[][] Q_amp;
    private static BigDecimal[][] Q_t;
    private static BigDecimal[][] Q_index;

    private static BigDecimal[][] J_amp_ECG;
    private static BigDecimal[][] J_amp;
    private static BigDecimal[][] J_t;
    private static BigDecimal[][] J_index;

    private static BigDecimal[][] T_peak_index;
    private static BigDecimal[][] T_peak_ECG;
    private static BigDecimal[][] T_peak_t;

    private static BigDecimal[][] TOFF_index;
    private static BigDecimal[][] TOFF_t;
    private static BigDecimal[][] TOFF_amp_ECG;

    private static BigDecimal[][] K_amp_ECG;
    private static BigDecimal[][] K_amp;
    private static BigDecimal[][] K_t;
    private static BigDecimal[][] K_index;

    private static BigDecimal[][] P_amp_ECG;
    private static BigDecimal[][] P_amp;
    private static BigDecimal[][] P_t;
    private static BigDecimal[][] P_index;

    private static BigDecimal[][] P_ON_index;
    private static BigDecimal[][] P_ON_amp;
    private static BigDecimal[][] P_ON_t;
    private static BigDecimal[][] P_ON_amp_ECG;

    private static BigDecimal[][] ISO_ECG;
    private static BigDecimal[][] ST_ECG;

    private static BigDecimal[][] DELTA_ST1;
    private static BigDecimal[][] DELTA_T1;

    private static BigDecimal ST_REF_ECG;
    private static BigDecimal T_REF_ECG;

    private static BigDecimal[][] DELTA_ST_index;
    private static BigDecimal[][] DELTA_ST_t;
    private static BigDecimal[][] DELTA_ST;
    private static BigDecimal[][] ST_ECG_DELTA_OV;
    private static BigDecimal[][] ST_ECG_DELTA_ISO;

    private static BigDecimal[][] DELTA_T_index;
    private static BigDecimal[][] DELTA_T_t;
    private static BigDecimal[][] DELTA_T;
    private static BigDecimal[][] T_ECG_DELTA_ISO;
    private static BigDecimal[][] T_ECG_DELTA;

    /**
     * Calcula la tasa cardiaca en cada minuto tomando el valor promedio entre
     * picos (60 segundos / (tiempo/latido))
     */
    private static BigDecimal H_R;

    public static void main(String[] args) {

    	System.out.println(sdf.format(new Date()));
    	wavalet = readFile("wavalet.csv");
    	String[] d = readFile("data.csv");
    	SAMPLES2READ = d.length;
        filsig1 = new BigDecimal[1][SAMPLES2READ];
        
        filsig1[0][0] = point(num(d[0]));
        filsig1[0][1] = point((num(d[0])+num(d[1]))/2);
        filsig1[0][2] = point((num(d[0])+num(d[1])+num(d[2]))/3);
        filsig1[0][3] = point((num(d[0])+num(d[1])+num(d[2])+num(d[3]))/4);
        
        for (int i = 4; i < SAMPLES2READ; i++) {
        	 filsig1[0][i] = point((num(d[i])+num(d[i-1])+num(d[i-2])+num(d[i-3])+num(d[i-4]))/5);
		}
        
        NoiseFilter filter = new NoiseFilter();

        tn = createRow(0, SAMPLES2READ - 1);
        filter.divide(sfreq, tn, DECIMAL_PRECISION);
        printMatrix(tn, "tn");

        // PASO 2
        filsig2 = filter.medfil_eod(filsig1, 200);
        printMatrix(filsig2, "filsig2");
        // PASO 3
        filsig3 = filter.medfil_eod(filsig2, 600);
        printMatrix(filsig3, "filsig3");

        // PASO 4
        D_ACOND = filter.substract(filsig1, filsig3);
        printMatrix(D_ACOND, "D_ACOND");
        // PASO 5
        x = filter.cwt_eod(D_ACOND);
         printMatrix(x, "x");

        detect_R_Peaks();

        detect_Heart_Rate();

        detect_S_Peaks();

        detect_Q_Peaks();

        detect_J_Peaks();

        detect_T_Peaks();

        detect_T_Peaks_Offset();

        detect_K_Peaks();

        detect_P_Peaks();

        detect_P_ON_SET_Peaks();

        isoelectric_line();

        calculate_amp();

        calculate_T_peak_ST_REF();

        calculate_desviation_ST_wave();

        calculate_desviation_T_wave();
        
        int condition_HR = 0;
        if (H_R.doubleValue()>=55 && H_R.doubleValue()<=115){
            condition_HR = 1;
        }else if (H_R.doubleValue()<55){
            condition_HR=0;
        }else {
        	condition_HR=2;
        }
        
        int RR_length = R_index[0].length-1;
		BigDecimal[][] RR = new BigDecimal[1][RR_length];
		BigDecimal constant = new BigDecimal(1000/360);
//        System.out.println(RR_length);
        for (int i=0;i < RR_length;i++){
			RR[0][i] = R_index[0][i+1].subtract(R_index[0][i]).multiply(constant);
        }
        
        BigDecimal[][] RRprima = new BigDecimal[1][RR_length];
        BigDecimal[][] RRnorm = new BigDecimal[1][RR_length-1];
        RRprima[0][0]=RR[0][0];
        
       
        for (int numero=0;numero < RR_length-1;numero++){
//        	RRprima(numero+1)=0.75*RRprima(numero)+0.25*RR(numero+1);
        	RRprima[0][numero+1] = point((RRprima[0][numero].doubleValue()*0.75)+0.25*RR[0][numero+1].doubleValue());
//        	RRnorm(numero)=(RR(numero)*100)/(RRprima(numero));
        	RRnorm[0][numero]= point(RR[0][numero].doubleValue()*100/RRprima[0][numero].doubleValue());
        }
//        printMatrix2(RRprima, "RRprima");
//        printMatrix2(RRnorm, "RRnorm");
        
        
        BigDecimal[][] desviacion = new BigDecimal[1][RR_length-1];
        double meanRRnorm = mean(RRnorm);
//        System.out.println("meanRRnorm="+meanRRnorm);
        for (int numero=0;numero < RR_length-1;numero++){
			desviacion[0][numero] = point(Math.pow((RRnorm[0][numero].doubleValue() - meanRRnorm),2));
        }
//        printMatrix2(desviacion, "desviacion");
        
        double varianza = mean(desviacion);
        
        int irregularidad = 0;
        if (varianza < 70){
            irregularidad = 0;
        }else{ 
        	irregularidad = 1;
        }
        
//        numero=1;
//        for numero=1:length(Q_index)-1
//                QRS(numero) = (S_index(numero) - Q_index(numero))*(1000/360);
//          
//        end
//        promedio_QRS= mean(QRS);
        int Q_index_length = Q_index[0].length;
        BigDecimal[][] QRS = new BigDecimal[1][Q_index_length-1];
		for (int numero=0;numero < Q_index_length-1;numero++){
			QRS[0][numero] = S_index[0][numero].subtract(Q_index[0][numero]).multiply(constant);
			
        }
		double promedio_QRS = mean(QRS);
		
//		printMatrix2(QRS, "QRS");
		
		int condition_QRS = 0;
		if (promedio_QRS < 127) {
			condition_QRS = 1;
		} else {
			condition_QRS = 0;
		}
		
//		numero=1;
//		for numero=1:length(TOFF_index)-1
//		    QT(numero) = (TOFF_index(numero) - Q_index(numero))*(1000/360);        
//		end
		int TOFF_index_length = TOFF_index[0].length;
		BigDecimal[][] QT = new BigDecimal[1][TOFF_index_length-1];
		for (int numero=0;numero < TOFF_index_length-1;numero++){
			QT[0][numero] = TOFF_index[0][numero].subtract(Q_index[0][numero]).multiply(constant);
		}
		double promedio_QT = mean(QT);
		int condition_QT = 0;
		if (promedio_QT < 440) {
			condition_QT = 1;
		} else {
			condition_QT = 0;
		}

//		%DETECCION DE DURACION DE COMPLEJO PR
//		numero=1;
//		for numero=1:length(P_ON_index)-1
//		    PR(numero) = (Q_index(numero) - P_ON_index(numero))*(1000/360);        
//		end
//		promedio_PR = mean(PR);
		int P_ON_index_length = Q_index[0].length;
		BigDecimal[][] PR = new BigDecimal[1][P_ON_index_length-1];
		for (int numero=0;numero < P_ON_index_length-1;numero++){
			PR[0][numero] = Q_index[0][numero].subtract(P_ON_index[0][numero]).multiply(constant);
		}
		double promedio_PR = mean(PR);
		int condition_PR = 0;
		if (promedio_PR>=120 && promedio_PR<=220){
		    condition_PR = 1;
		}else if (promedio_PR>220){
		    condition_PR = 2;
		}else{ 
			condition_PR = 0;
		}

//		==============================
//		Ppromedio = mean(1000*P_amp_ECG);
//		Rpromedio = mean(1000*R_amp_ECG);
//		PentreR = Ppromedio/Rpromedio;
		
		double Ppromedio = mean(P_amp_ECG);
		double Rpromedio = mean(R_amp_ECG);
//		double PentreR = Ppromedio/Rpromedio;
//		 
//		for numero=1:length(P_ON_index)-1
//		            TamanoP(numero) = (P_amp_ECG(numero) - P_ON_amp_ECG(numero))*1000;
//		end
//		 
//		for numero=1:length(R_index)-1
//		          TamanoR(numero) = (R_amp_ECG(numero) - Q_amp_ECG(numero))*1000;
//		end
		
		int R_index_length = R_index[0].length;
		constant = point(1000);
		BigDecimal[][] TamanoR = new BigDecimal[1][R_index_length-1];
		for (int numero=0;numero < R_index_length-1;numero++){
			TamanoR[0][numero] = R_amp_ECG[0][numero].subtract(Q_amp_ECG[0][numero]).multiply(constant);
		}
//		 
//		TamanoP = mean(TamanoP);
		
		double TamanoRMean = mean(TamanoR);
//		PentreR =TamanoP/TamanoR;
//		 
//		if (PentreR>=0.1)
//		    condition_P = 1;
//		else condition_P = 0;
//		end
		
		
//		%/DETECCION DE ARRITMIA
//
//		%%%%% RITMO SINUSAL NORMAL %%%%%%%
		String ARRITMIA = "RSN";// %Valor inicial

//		%%%%% ASISTOLIA %%%%%%%%%%%%%
		if (TamanoRMean < 1) {
			ARRITMIA = "ASISTOLIA";   // //Asistolia

		}else if (irregularidad==0 && condition_HR==1 && condition_PR==1 && condition_QRS==1 && condition_QT==1){
			ARRITMIA = "RSN";   //Ritmo sinusal normal

//		%%%%//BRADICARDIA SINUSAL %%%%%%%%
		}else if (irregularidad==0 && condition_HR==0 && condition_PR==1 && condition_QRS==1 && condition_QT==1){
			ARRITMIA = "Bradicardia Sinusal";

//		%%%%//TAQUICARDIA SINUSAL %%%%%%%%
		}else if (irregularidad==0 && condition_HR==2 && condition_PR==1 && condition_QRS==1 && condition_QT==1){
			ARRITMIA = "Taquicardia Sinusal";

//		%%%%//ARRITMIA SINUSAL %%%%%%%%%%%
		}else if (irregularidad==1 && condition_PR==1 && condition_QRS==1 && condition_QT==1){
			ARRITMIA = "Arritmia Sinusal Normal";

//		%%%%//FLUTTER ATRIAL %%%%%%%%%%%%%
		}else if (irregularidad==0 && condition_PR==0 && condition_QRS==1 && condition_QT==1){
			ARRITMIA = "Flutter Atrial";

//		%%%%//FIBRILACION ATRIAL %%%%%%%%%
		}else if (irregularidad==1 && condition_PR==0 && condition_QRS==1 && condition_QT==1){
			ARRITMIA = "Fibrilacion Atrial";

//		%%%%//BLOQUEO AV DE PRIMER GRADO %
		}else if (irregularidad==0 && condition_PR==2 && condition_QRS==1 && condition_QT==1){
			ARRITMIA = "Bloqueo AV de Primer Grado";

//		%%%//TORSADA DE PUNTAS %%%%%%%%%%%
		}else if (irregularidad==0&& condition_PR==1 && condition_QRS==1 && condition_QT==0){
			ARRITMIA = "Torsada de Puntas";

//		%%%%//FIBRILACION VENTRICULAR %%%%
		}else if (irregularidad==1 && condition_HR ==2 && condition_PR==0 && condition_QRS==1 && condition_QT==1){
			ARRITMIA = "Fibrilacion Ventricular";

//		%%%%%//OTRA ARRITMIA
		}else{
			ARRITMIA = "Otra arritmia";   
		}


		if (ARRITMIA.length() == 3 ){//%"RSN"
		//No hacer nada
		}else{ 
//			%Enviar alarma 
//			%Se envía una alarma con el valor de la variable ARRITMIA y DNI del paciente al celular del médico tratante. Además se envía la señal generada es decir el archivo .csv al servidor, con la fecha y hora del evento detectado.
			//El DNI del paciente y cel del médico te los da la APP que hizo Kevin.
			System.out.println(ARRITMIA);
		}


        System.out.println(sdf.format(new Date()));
		
    }

	private static double num(String d) {
		return Double.parseDouble(d);
	}

    private static void isoelectric_line() {
        Integer T_len = T_peak_index[0].length - 1;
        ISO_ECG = new BigDecimal[1][T_len];

        for (int j = 1; j <= T_len; j++) {
            int toffIndex = TOFF_index[0][j - 1].intValue();
            int pOnIndex = P_ON_index[0][j].intValue();
            if ((toffIndex - pOnIndex) >= 0) {
                ISO_ECG[0][j - 1] = point(0);
            } else {
                ISO_ECG[0][j - 1] = point(mean(limitRow(D_ACOND, toffIndex, pOnIndex)));
            }

//			ISO_ECG[0][j-1] = point(mean(limitRow(D_ACOND, pOnIndex,toffIndex)));
        }
//		System.out.println("WARNING:mock");
        printMatrix(ISO_ECG, "ISO_ECG");
    }

    private static void calculate_desviation_T_wave() {
        Integer Contador_delta_T = 0;
        Integer Contador_delta_T_true = 0;

        Integer len_ISO = ISO_ECG[0].length;
        DELTA_T1 = new BigDecimal[1][len_ISO];

        DELTA_T_index = new BigDecimal[1][len_ISO];
        DELTA_T_t = new BigDecimal[1][len_ISO];
        DELTA_T = new BigDecimal[1][len_ISO];
        T_ECG_DELTA_ISO = new BigDecimal[1][len_ISO];
        T_ECG_DELTA = new BigDecimal[1][len_ISO];
        Integer jj = 0;
        Integer k = 0;
        for (int j = 1; j <= len_ISO; j++) {
            DELTA_T1[0][j - 1] = T_peak_ECG[0][j - 1].subtract(ISO_ECG[0][j - 1]).subtract(T_REF_ECG);

            if (DELTA_T1[0][j - 1].abs().doubleValue() > 0.0002) {  //TODO 0.0002
                Contador_delta_T = Contador_delta_T + 1;
                if (Contador_delta_T == 1) {
                    jj = j;
                }
            } else {
                if (Contador_delta_T < 30) {
                    Contador_delta_T = 0;

                } else {
                    Contador_delta_T_true = Contador_delta_T_true + 1;

//		            if(k >= Contador_delta_T){
//		            	k=Contador_delta_T-1;
//		            }

                    for (k = k + 1; k <= Contador_delta_T; k++) {
//                        System.out.println("j=" + j);
                        DELTA_T_index[0][k - 1] = T_peak_index[0][j - 1];
                        DELTA_T_t[0][k - 1] = tn[0][T_peak_index[0][j - 1].intValue()];
                        DELTA_T[0][k - 1] = DELTA_T1[0][j - 1];
                        T_ECG_DELTA_ISO[0][k - 1] = T_peak_ECG[0][jj - 1].subtract(ISO_ECG[0][jj - 1]);
                        T_ECG_DELTA[0][k - 1] = T_peak_ECG[0][jj - 1];
                    }

                }
            }
        }

        printMatrix(DELTA_T1, "DELTA_T1");

        DELTA_T_index = clean(DELTA_T_index);
        printMatrix(DELTA_T_index, "DELTA_T_index");

        DELTA_T_t = clean(DELTA_T_t);
        printMatrix(DELTA_T_t, "DELTA_T_t");

        DELTA_T = clean(DELTA_T);
        printMatrix(DELTA_T, "DELTA_T");

        T_ECG_DELTA_ISO = clean(T_ECG_DELTA_ISO);
        printMatrix(T_ECG_DELTA_ISO, "T_ECG_DELTA_ISO");

        T_ECG_DELTA = clean(T_ECG_DELTA);
        printMatrix(T_ECG_DELTA, "T_ECG_DELTA");
    }

    private static void calculate_desviation_ST_wave() {

        Integer Contador_delta_ST = 0;
        Integer Contador_delta_ST_true = 0;

        Integer len_ISO = ISO_ECG[0].length;
        DELTA_ST1 = new BigDecimal[1][len_ISO];

        DELTA_ST_index = new BigDecimal[1][len_ISO];
        DELTA_ST_t = new BigDecimal[1][len_ISO];
        DELTA_ST = new BigDecimal[1][len_ISO];
        ST_ECG_DELTA_OV = new BigDecimal[1][len_ISO];
        ST_ECG_DELTA_ISO = new BigDecimal[1][len_ISO];

        Integer jj = 0;
        Integer k = 0;
        for (int j = 1; j <= len_ISO; j++) {

            DELTA_ST1[0][j - 1] = ST_ECG[0][j - 1].subtract(ISO_ECG[0][j - 1]).subtract(ST_REF_ECG);

            if (DELTA_ST1[0][j - 1].abs().doubleValue() > 0.0001) {
                Contador_delta_ST = Contador_delta_ST + 1;

                if (Contador_delta_ST == 1) {
                    jj = j;

                }
            } else {
                if (Contador_delta_ST < 30) {
                    Contador_delta_ST = 0;

                } else {
                    Contador_delta_ST_true = Contador_delta_ST_true + 1;

                    if (k >= Contador_delta_ST) {
                        k = Contador_delta_ST - 1;
                    }

                    for (k = k + 1; k <= Contador_delta_ST; k++) {
                        DELTA_ST_index[0][k - 1] = point(J_index[0][jj - 1].intValue() + 20);
                        DELTA_ST_t[0][k - 1] = tn[0][J_index[0][jj - 1].intValue() + 20 - 1];
                        DELTA_ST[0][k - 1] = DELTA_ST1[0][j - 1];
                        ST_ECG_DELTA_OV[0][k - 1] = ST_ECG[0][jj - 1];
                        ST_ECG_DELTA_ISO[0][k - 1] = ST_ECG[0][jj - 1].subtract(ISO_ECG[0][jj - 1]);
                    }

                }
            }
        }

        printMatrix(DELTA_ST1, "DELTA_ST1");
        DELTA_ST_index = clean(DELTA_ST_index);
        printMatrix(DELTA_ST_index, "DELTA_ST_index");
        DELTA_ST_t = clean(DELTA_ST_t);
        printMatrix(DELTA_ST_t, "DELTA_ST_t");
        DELTA_ST = clean(DELTA_ST);
        printMatrix(DELTA_ST, "DELTA_ST");
        ST_ECG_DELTA_OV = clean(ST_ECG_DELTA_OV);
        printMatrix(ST_ECG_DELTA_OV, "ST_ECG_DELTA_OV");
        ST_ECG_DELTA_ISO = clean(ST_ECG_DELTA_ISO);
        printMatrix(ST_ECG_DELTA_ISO, "ST_ECG_DELTA_ISO");
    }

    /**
     * valores manuales
     */
    private static void calculate_T_peak_ST_REF() {
        ST_REF_ECG = point(0);

        int length = 30;

        if (ISO_ECG[0].length < length) {
            length = ISO_ECG[0].length;
        }

        for (int j = 1; j <= length; j++) {
            ST_REF_ECG = D_ACOND[0][J_index[0][j - 1].intValue() + 20 - 1].subtract(ISO_ECG[0][j - 1]).add(ST_REF_ECG);
        }
        ST_REF_ECG = ST_REF_ECG.divide(point(length),DECIMAL_PRECISION,RoundingMode.FLOOR);
        System.out.println("=================================================");
        System.out.println("ST_REF_ECG=" + ST_REF_ECG);

        T_REF_ECG = point(0);
        for (int j = 1; j <= length; j++) {
            T_REF_ECG = T_peak_ECG[0][j - 1].subtract(ISO_ECG[0][j - 1]).add(T_REF_ECG);
        }
        T_REF_ECG = T_REF_ECG.divide(point(length), DECIMAL_PRECISION,RoundingMode.FLOOR);
        System.out.println("=================================================");
        System.out.println("T_REF_ECG=" + T_REF_ECG);

    }

    private static void calculate_amp() {
        Integer len_ISO = ISO_ECG[0].length;
        ST_ECG = new BigDecimal[1][len_ISO];
        for (int j = 1; j <= len_ISO; j++) {
            ST_ECG[0][j - 1] = D_ACOND[0][J_index[0][j - 1].intValue() + 20 - 1];
        }
        printMatrix(ST_ECG, "ST_ECG");
    }


    private static void detect_P_ON_SET_Peaks() {
        Integer P_len = P_index[0].length;
        Integer IP1 = null;
//		Integer found=null;
        BigDecimal P_pico = null;

        P_ON_index = new BigDecimal[1][P_len];
        P_ON_amp = new BigDecimal[1][P_len];
        P_ON_t = new BigDecimal[1][P_len];
        P_ON_amp_ECG = new BigDecimal[1][P_len];

        Integer i_ON = null;
        for (int j = 1; j <= P_len; j++) {
            IP1 = P_index[0][j - 1].intValue();
            P_pico = D_ACOND[0][IP1 - 1];
//		    found = 0;
            int k = IP1 - getFactor(0.08);
            for (int i = IP1; i >= k; i--) {

                if (i > 1) {
                    BigDecimal max = D_ACOND[0][i - 2].abs();
                    if (max.doubleValue() < P_pico.doubleValue()) {
                        P_pico = max;
                        i_ON = i - 1;
//			                found = 1;
                    } else {
                        i_ON = i;
                    }
                } else if (i == 1) {
                    i_ON = i;
                    break;
                } else {
                    i_ON = i;
                }

            }
            P_ON_index[0][j - 1] = point(i_ON);
            P_ON_amp[0][j - 1] = x[i_ON - 1][0];
            P_ON_t[0][j - 1] = tn[0][i_ON - 1];
            P_ON_amp_ECG[0][j - 1] = D_ACOND[0][i_ON - 1];
        }
        printMatrix(P_ON_index, "P_ON_index");
        printMatrix(P_ON_amp, "P_ON_amp");
        printMatrix(P_ON_t, "P_ON_t");
        printMatrix(P_ON_amp_ECG, "P_ON_amp_ECG");
    }

    private static void detect_P_Peaks() {
        Integer K_len = K_index[0].length;
        Integer IK1 = null;
        Integer i_max = null;
        BigDecimal P_max = null;

        P_index = new BigDecimal[1][K_len];
        P_amp = new BigDecimal[1][K_len];
        P_t = new BigDecimal[1][K_len];
        P_amp_ECG = new BigDecimal[1][K_len];


        for (int j = 1; j <= K_len; j++) {
            IK1 = K_index[0][j - 1].intValue();
            P_max = D_ACOND[0][IK1 - 1];

            int k = IK1 - getFactor(0.16);

            for (int i = IK1; i >= k; i--) {

                if (i > 1) {
                    BigDecimal max = D_ACOND[0][i - 2];
                    if (max.doubleValue() > P_max.doubleValue()) {
                        P_max = max;
                        i_max = i - 1;
                    }
                } else if (i == 1) {
                    i_max = i;
                    break;
                }


            }

            P_index[0][j - 1] = point(i_max);
            P_amp[0][j - 1] = x[i_max - 1][0];
            P_t[0][j - 1] = tn[0][i_max - 1];
            P_amp_ECG[0][j - 1] = D_ACOND[0][i_max - 1];

        }
        printMatrix(P_index, "P_index");
        printMatrix(P_amp, "P_amp");
        printMatrix(P_t, "P_t");
        printMatrix(P_amp_ECG, "P_amp_ECG");
    }

    private static void detect_K_Peaks() {
        Integer Q_len = Q_index[0].length;
        Integer IQ1 = null;
        Integer foundk = null;

        K_index = new BigDecimal[1][Q_len];
        K_amp = new BigDecimal[1][Q_len];
        K_t = new BigDecimal[1][Q_len];
        K_amp_ECG = new BigDecimal[1][Q_len];

        Integer i = 0;
        for (int j = 1; j <= Q_len; j++) {

            IQ1 = Q_index[0][j - 1].intValue();
            foundk = 0;
            i = IQ1;

            while ((i > 0)
                    && (i > (IQ1.doubleValue() - getFactor(0.06)))) {

                if (D_ACOND[0][i - 1].doubleValue() >= 0) {
                    K_index[0][j - 1] = point(i);
                    K_amp[0][j - 1] = x[i - 1][0];
                    K_t[0][j - 1] = tn[0][i - 1];
                    K_amp_ECG[0][j - 1] = D_ACOND[0][i - 1];
                    foundk = 1;
                    break;
                }

                i = i - 1;
            }

            if (foundk == 0) {
                K_index[0][j - 1] = point(i + 1);
                K_amp[0][j - 1] = x[i][0];
                K_t[0][j - 1] = tn[0][i];
                K_amp_ECG[0][j - 1] = D_ACOND[0][i];
            }

        }
        printMatrix(K_index, "K_index");
        printMatrix(K_amp, "K_amp");
        printMatrix(K_t, "K_t");
        printMatrix(K_amp_ECG, "K_amp_ECG");
    }

    private static void detect_T_Peaks_Offset() {
        Integer T_len = T_peak_index[0].length;
        Integer i_min = null;
        Integer IT1 = null;
        BigDecimal min_abs = null;

        TOFF_index = new BigDecimal[1][T_len];
        TOFF_t = new BigDecimal[1][T_len];
        TOFF_amp_ECG = new BigDecimal[1][T_len];


        for (int j = 1; j <= T_len; j++) {
            IT1 = T_peak_index[0][j - 1].intValue();
            min_abs = D_ACOND[0][IT1 - 1].abs();

            for (int i = IT1; i <= IT1 + getFactor(0.13); i++) {
                if (i < SAMPLES2READ) {
                    if (D_ACOND[0][i].abs().doubleValue() < min_abs.doubleValue()) {
                        min_abs = D_ACOND[0][i].abs();
                        i_min = i + 1;
                    } else {
                        i_min = i;
                    }
                }
            }
            TOFF_index[0][j - 1] = point(i_min);
            TOFF_t[0][j - 1] = tn[0][i_min - 1];
            TOFF_amp_ECG[0][j - 1] = D_ACOND[0][i_min - 1];
        }

        printMatrix(TOFF_index, "TOFF_index");
        printMatrix(TOFF_t, "TOFF_index");
        printMatrix(TOFF_amp_ECG, "TOFF_amp_ECG");
    }

    private static void detect_T_Peaks() {
        Integer J_len = J_index[0].length;
        Integer P1 = null;
        Integer P2 = null;

        T_peak_index = new BigDecimal[1][J_len];
        T_peak_ECG = new BigDecimal[1][J_len];
        T_peak_t = new BigDecimal[1][J_len];

        BigDecimal ubicacion = null;
        BigDecimal index = null;
        for (int j = 1; j <= J_len; j++) {
            P1 = R_index[0][j - 1].intValue() + getFactor(0.6);
            P2 = J_index[0][j - 1].intValue() + getFactor(0.04);

            if (P1 > x.length) {
                P1 = x.length;
            }

            if (P2 > x.length) {
                P2 = x.length;
            }

            if (P1 > P2) {
                ubicacion = maxValuePosition(abs(limitRow(D_ACOND, P2, P1)));
                index = ubicacion.add(point(P2).subtract(point(1)));
                T_peak_index[0][j - 1] = index;
                T_peak_ECG[0][j - 1] = D_ACOND[0][index.intValue() - 1];
                T_peak_t[0][j - 1] = tn[0][index.intValue() - 1];
            }
        }
        printMatrix(T_peak_index, "T_peak_index");
        printMatrix(T_peak_ECG, "T_peak_ECG");
        printMatrix(T_peak_t, "T_peak_t");
    }


    private static void detect_J_Peaks() {
        Integer S_len = S_index[0].length;

        Integer len_buscar = getFactor(0.06);
        Integer IS1 = null;
        Integer k = null;

        BigDecimal[][] d1D_ACOND = new BigDecimal[1][len_buscar + 1];
        BigDecimal[][] d2D_ACOND = new BigDecimal[1][len_buscar];

        J_index = new BigDecimal[1][S_len];
        J_amp = new BigDecimal[1][S_len];
        J_t = new BigDecimal[1][S_len];
        J_amp_ECG = new BigDecimal[1][S_len];

        for (int j = 1; j <= S_len; j++) {
            IS1 = S_index[0][j - 1].intValue();
            k = 0;
            for (int i = IS1; i <= IS1 + len_buscar; i++) {
                if ((i < SAMPLES2READ) && (i > 0)) {
                    k = k + 1;
                    d1D_ACOND[0][k - 1] = D_ACOND[0][i].subtract(D_ACOND[0][i - 1]);
                }
            }
            if (k != 0) {
                for (k = 1; k <= d1D_ACOND[0].length - 1; k++) {
                    d2D_ACOND[0][k - 1] = d1D_ACOND[0][k].subtract(d1D_ACOND[0][k - 1]);
                }
                Integer signo1 = null;
                if (d2D_ACOND[0][0].doubleValue() > 0) {
                    signo1 = 0;
                } else {
                    signo1 = 1;
                }

                for (int i = 1; i <= d2D_ACOND[0].length - 1; i++) {
                    Integer signo2 = null;
                    if (d2D_ACOND[0][i].doubleValue() > 0) {
                        signo2 = 0;
                    } else {
                        signo2 = 1;
                    }

                    if (!signo1.equals(signo2)) {
                        signo1 = signo2;
                        J_index[0][j - 1] = point(IS1 + i);
                        J_amp[0][j - 1] = x[IS1 + i - 1][0];
                        J_t[0][j - 1] = tn[0][IS1 + i - 1];
                        J_amp_ECG[0][j - 1] = D_ACOND[0][IS1 + i - 1];

                    } else if (i == d2D_ACOND[0].length - 1) {
                        J_index[0][j - 1] = point(IS1 + i);
                        J_amp[0][j - 1] = x[IS1 + i - 1][0];
                        J_t[0][j - 1] = tn[0][IS1 + i - 1];
                        J_amp_ECG[0][j - 1] = D_ACOND[0][IS1 + i - 1];
                    }

                }


            }
        }
        d1D_ACOND = null;
        d2D_ACOND = null;

        J_index = clean(J_index);
        printMatrix(J_index, "J_index");

        J_amp = clean(J_amp);
        printMatrix(J_amp, "J_amp");

        J_t = clean(J_t);
        printMatrix(J_t, "J_t");

        J_amp_ECG = clean(J_amp_ECG);
        printMatrix(J_amp_ECG, "J_amp_ECG");

    }

    private static int getFactor(double factor) {
        return point(
                sfreq.doubleValue() * factor * (H_R.doubleValue() / 72))
                .setScale(0, RoundingMode.HALF_EVEN).intValue();
    }


    public static BigDecimal[][] clean(BigDecimal[][] v) {
        List<BigDecimal> list = new ArrayList<BigDecimal>();
        for (int i = 0; i < v[0].length; i++) {
            if (v[0][i] != null) {
                list.add(v[0][i]);
            }
        }
        return listToMatrixRow(list);
    }

    private static void detect_Q_Peaks() {
        Integer R_len = R_index[0].length;
        Integer rango = getFactor(0.06);

        BigDecimal ii = null;
        BigDecimal IR1 = null;
        Integer Found = null;
        Q_index = new BigDecimal[1][R_len];
        Q_amp = new BigDecimal[1][R_len];
        Q_t = new BigDecimal[1][R_len];
        Q_amp_ECG = new BigDecimal[1][R_len];


        for (int j = 1; j <= R_len; j++) {
            IR1 = R_index[0][j - 1];
            Found = 0;
            int i = 0;
            int k = IR1.intValue() - rango.intValue();

            for (i = IR1.intValue(); i >= k; i--) {
                if ((i < SAMPLES2READ) && (i > 1)) {
                    if ((D_ACOND[0][i - 1].doubleValue() < D_ACOND[0][i]
                            .doubleValue())
                            && (D_ACOND[0][i - 1].doubleValue() < D_ACOND[0][i - 2]
                            .doubleValue())) {

                        Q_index[0][j - 1] = point(i);
                        Q_amp[0][j - 1] = x[i - 1][0];
                        Q_t[0][j - 1] = tn[0][i - 1];
                        Q_amp_ECG[0][j - 1] = D_ACOND[0][i - 1];
                        Found = 1;
                    }

                } else if ((i == 0) || (i == SAMPLES2READ)) {

                    Q_index[0][j - 1] = point(i + 1);
                    Q_amp[0][j - 1] = x[i][0];
                    Q_t[0][j - 1] = tn[0][i];
                    Q_amp_ECG[0][j - 1] = D_ACOND[0][i];
                    Found = 1;
                }
            }
            if (Found == 0) {
                ii = point(i + 1
                        - point(rango / 2).setScale(0,
                        RoundingMode.HALF_EVEN).intValue());
                Q_index[0][j - 1] = ii;
                Q_amp[0][j - 1] = x[ii.intValue() - 1][0];
                Q_t[0][j - 1] = tn[0][ii.intValue() - 1];
                Q_amp_ECG[0][j - 1] = D_ACOND[0][ii.intValue() - 1];
            }

        }
        printMatrix(Q_index, "Q_index");

        printMatrix(Q_amp, "Q_amp");

        printMatrix(Q_t, "Q_t");

        printMatrix(Q_amp_ECG, "Q_amp_ECG");
    }

    private static void detect_S_Peaks() {
        // % Detecci�n de los puntos S
        Integer R_len = R_index[0].length;
//		Integer rango = point(
//				sfreq.doubleValue() * 0.15 * (H_R.doubleValue() / 72))
//				.setScale(0, RoundingMode.HALF_EVEN).intValue();
        Integer rango = getFactor(0.15);
        Integer IR1 = null;
        Integer Found = null;

        S_index = new BigDecimal[1][R_len];
        S_amp = new BigDecimal[1][R_len];
        S_t = new BigDecimal[1][R_len];
        S_amp_ECG = new BigDecimal[1][R_len];

        for (int j = 1; j <= R_len; j++) {
            IR1 = R_index[0][j - 1].intValue();
            Found = 0;

            for (int i = IR1; i <= IR1 + rango; i++) {
                if ((0 < i) && (i < SAMPLES2READ) && (Found == 0)) {
                    if ((D_ACOND[0][i - 1].doubleValue() < D_ACOND[0][i]
                            .doubleValue())
                            && (D_ACOND[0][i - 1].doubleValue() < D_ACOND[0][i - 2]
                            .doubleValue())) {
                        S_index[0][j - 1] = (point(i));
                        S_amp[0][j - 1] = (x[i - 1][0]);
                        S_t[0][j - 1] = (tn[0][i - 1]);
                        S_amp_ECG[0][j - 1] = (D_ACOND[0][i - 1]);
                        Found = 1;

                    } else if ((i == IR1 + rango) & Found == 0) {
                        S_index[0][j - 1] = (point(i));
                        S_amp[0][j - 1] = (x[i - 1][0]);
                        S_t[0][j - 1] = (tn[0][i - 1]);
                        S_amp_ECG[0][j - 1] = (D_ACOND[0][i - 1]);
                        Found = 1;
                    }

                } else if ((i == 0) || (i == SAMPLES2READ)) {
                    S_index[0][j - 1] = (point(i));
                    S_amp[0][j - 1] = (x[i - 1][0]);
                    S_t[0][j - 1] = (tn[0][i - 1]);
                    S_amp_ECG[0][j - 1] = (D_ACOND[0][i - 1]);
                }

            }
        }

        printMatrix(S_index, "S_index");
        printMatrix(S_amp, "S_amp");
        printMatrix(S_t, "S_t");
        printMatrix(S_amp_ECG, "S_amp_ECG");
    }

    /**
     * C�lculo de la tasa cardiaca
     */
    private static void detect_Heart_Rate() {
        List<BigDecimal> HRRList = new ArrayList<BigDecimal>();
        for (int j = 2; j <= R_t[0].length - 1; j++) {
            HRRList.add(R_t[0][j - 1].subtract(R_t[0][j - 2]));
        }
        BigDecimal[][] HRRArray = listToMatrixRow(HRRList);
        printMatrix(HRRArray, "HRR");
        H_R = point(60 / mean(HRRArray));
        System.out.println("H_R=" + H_R);
//        Log.d("Wilder","H_R=" + H_R);
    }

    public static double mean(BigDecimal[][] m) {
        double sum = 0;
        for (int i = 0; i < m[0].length; i++) {
            sum += m[0][i].doubleValue();
        }
        return sum / m[0].length;
    }

    /**
     * DETECCION DE LOS PICOS R
     */
    private static void detect_R_Peaks() {
        Double thresh = 0.2;// %Umbral
        BigDecimal len = point(filsig1[0].length);
        BigDecimal offset = len.divide(point(4));
        Integer start = round(offset);
        Integer end = round(offset.multiply(point(3)));

        BigDecimal max_h = maxValueMatrixColumn(x, start, end);
        System.out.println("max_h=" + max_h);

        Integer i = 2;
        Integer k = 0;
        List<BigDecimal> R_tList = new ArrayList<BigDecimal>();
        List<BigDecimal> R_ampList = new ArrayList<BigDecimal>();
        List<BigDecimal> R_amp_ECGList = new ArrayList<BigDecimal>();
        List<BigDecimal> R_indexList = new ArrayList<BigDecimal>();

        while (i < len.intValue()) {
            if (x[i - 1][0].doubleValue() > thresh * max_h.doubleValue()) {
                while (x[i - 1][0].doubleValue() > 0 && i < len.intValue()) {
                    i++;
                }
                k = k + 1;
                int index = i - 1 - 1;
                R_indexList.add(point(index + 1));
                R_tList.add(tn[0][index]);
                R_ampList.add(x[index][0]);
                R_amp_ECGList.add(D_ACOND[0][index]);

                i = i + 20;
            } else {
                i++;
            }
        }
        R_index = listToMatrixRow(R_indexList);
        printMatrix(R_index, "R_index");

        R_t = listToMatrixColumn(R_tList);
        R_t = transposeMatrix(R_t);
        printMatrix(R_t, "R_t % Instantes en los que ocurre cada pico R");

        R_amp = listToMatrixColumn(R_ampList);
        R_amp = transposeMatrix(R_amp);
        printMatrix(R_amp, "R_amp % Picos R (sobre la curva CWT) ");

        R_amp_ECG = listToMatrixColumn(R_amp_ECGList);
        R_amp_ECG = transposeMatrix(R_amp_ECG);
        printMatrix(R_amp_ECG, "R_amp_ECG % Resultado Picos R");
    }

    private static Integer round(BigDecimal offset) {
        return offset.setScale(0, ROUNDING_MODE.HALF_UP).intValue();
    }

    public static BigDecimal maxValueMatrixColumn(BigDecimal[][] matrix,
                                                  Integer start, Integer end) {
        BigDecimal max = point(0);
        for (int i = start; i <= end; i++) {
            BigDecimal value = matrix[i - 1][0];
            if (value.compareTo(max) == 1) {
                max = value;
            }
        }
        return max;
    }


	private static ArrayList<Object> dataList;

    /**
     * TODO
     *
     * @return
     */
    public BigDecimal[][] intwave() {


        BigDecimal[][] row = new BigDecimal[1][wavalet.length];
        for (int i = 0; i < wavalet.length; i++) {
            row[0][i] = point(wavalet[i]);
        }
//        Log.d("Wilder","tamanio de wavalet: "+row[0].length);
        return row;


        //  return createRow(1, 8);
    }

    /**
     * %PASO 5 %Transformada wavelet continua de la se�al ECG
     *
     * @param D_ACOND
     * @return
     */
    public BigDecimal[][] cwt_eod(BigDecimal[][] D_ACOND) {
        // BigDecimal[][] D_ACONDT = transposeMatrix(D_ACOND);
        // printMatrix(D_ACONDT, "D_ACONDT");
        BigDecimal[][] ySIG = D_ACOND.clone();

        Integer lenSIG = ySIG[0].length;

        // BigDecimal[][] xSIG = createRow(1, lenSIG);
        // printMatrix(xSIG, "xSIG");
        BigDecimal stepSIG = point(1);
        BigDecimal[][] val_WAV = intwave();

        // printMatrix(val_WAV, "val_WAV");
        Integer lenWAV = val_WAV[0].length;

        Integer xMaxWAV = 1;
        // BigDecimal[][] xWAV = linspace(0, xMaxWAV, lenWAV);
        // printMatrix(xWAV, "xWAV");

        BigDecimal stepWAV = point(1 / (Double.parseDouble(lenWAV.toString()) - 1));
        System.out.println("stepWAV=" + stepWAV);
        // Integer wtype = 4;

        val_WAV = cumsum(val_WAV);
        multiply(stepWAV, val_WAV);
        // printMatrix(val_WAV, "val_WAV");
        // printMatrix(ySIG, "ySIG");

        Integer nb_SCALES = scales[0].length;
        BigDecimal[][] coefs = new BigDecimal[nb_SCALES][lenSIG];
        fillMatrixWith(coefs, point(0));
        // printMatrix(coefs, "coefs");

        Integer ind = 1;
        BigDecimal a = null;
        BigDecimal a_SIG = null;
        for (int i = 0; i < nb_SCALES; i++) {
            a = scales[0][i];
            a_SIG = a.divide(stepSIG);

            BigDecimal[][] j = createRow(0, a_SIG.intValue() * xMaxWAV);
            divide(a_SIG.multiply(stepWAV), j, 0);
            add(point(1), j);
            if (j[0].length == 1) {
                j = new BigDecimal[1][2];
                fillMatrixWith(j, point(1));
            }

            populateMatrixByIndexRowMatrix(j, val_WAV);
            fliplr_eod(j);
            // printMatrix(j, "j");

            BigDecimal[][] wconv1_eod = wconv1_eod(ySIG, j);
            // printMatrix(wconv1_eod, "wconv1_eod");

            BigDecimal[][] diff_eod = diff_eod(wconv1_eod);
            // printMatrix(diff_eod, "diff_eod");

            BigDecimal[][] wkeep1_eod = wkeep1_eod(diff_eod, lenSIG);
            // printMatrix(wkeep1_eod, "wkeep1_eod");

            multiply(point(-Math.sqrt(a.doubleValue())), wkeep1_eod);

            coefs[ind - 1] = wkeep1_eod[0];
            // printMatrix(coefs, "coefs");
            ind++;
        }
        // printMatrix(coefs, "coefs");

        BigDecimal[][] cwt_D_ACONDT = coefs.clone();
        // BigDecimal[][] cwt_D_ACONDTT = transposeMatrix(cwt_D_ACONDT);
        // printMatrix(cwt_D_ACONDTT, "cwt_D_ACONDTT");

        BigDecimal[][] x = new BigDecimal[1][_SCALE];
        x[0] = cwt_D_ACONDT[_SCALE - 1];

        return transposeMatrix(x);
    }

    @SuppressWarnings("static-access")
    public BigDecimal[][] wkeep1_eod(BigDecimal[][] diff_eod, Integer len) {
        List<BigDecimal> wkeep1_eodList = new ArrayList<BigDecimal>();
        Integer sx = diff_eod[0].length;
        double d = (sx.doubleValue() - len.doubleValue()) / 2;

        Integer first = point(d).add(point(1)).setScale(0, ROUNDING_MODE.FLOOR)
                .intValue();
        Integer last = point(sx).subtract(
                point(d).setScale(0, ROUNDING_MODE.CEILING)).intValue();

        for (int i = first; i <= last; i++) {
            wkeep1_eodList.add(diff_eod[0][i - 1]);
        }

        BigDecimal[][] wkeep1_eod = listToMatrixRow(wkeep1_eodList);

        return wkeep1_eod;
    }

    private static BigDecimal[][] listToMatrixRow(List<BigDecimal> list) {
        BigDecimal[][] matrix = new BigDecimal[1][list.size()];
        for (int flag = 0; flag < list.size(); flag++) {
            matrix[0][flag] = list.get(flag);
        }
        return matrix;
    }

    private static BigDecimal[][] listToMatrixColumn(List<BigDecimal> list) {
        BigDecimal[][] matrix = new BigDecimal[list.size()][1];
        for (int flag = 0; flag < list.size(); flag++) {
            matrix[flag][0] = point(list.get(flag).doubleValue());
        }
        return matrix;
    }

    public BigDecimal[][] diff_eod(BigDecimal[][] wconv1_eod) {
        BigDecimal[][] diff_eod = new BigDecimal[wconv1_eod.length][wconv1_eod[0].length - 1];

        for (int jc = 1; jc <= diff_eod[0].length; jc++) {
            diff_eod[0][jc - 1] = wconv1_eod[0][jc]
                    .subtract(wconv1_eod[0][jc - 1]);
        }
        return diff_eod;
    }

    public BigDecimal[][] wconv1_eod(BigDecimal[][] x, BigDecimal[][] f) {
        List<BigDecimal> yList = new ArrayList<BigDecimal>();
        BigDecimal[][] y = null;
        BigDecimal[][] a = x.clone();
        // printMatrix(a, "a");
        BigDecimal[][] b = f.clone();

        Integer ma = a.length;
        Integer na = a[0].length;

        Integer mb = b.length;
        Integer nb = b[0].length;

        Integer mc = 0;
        if (ma == 0 || mb == 0) {
            mc = ma + mb;
        } else {
            mc = ma + mb - 1;
        }

        Integer nc = 0;
        if (na == 0 || nb == 0) {
            nc = na + nb;
        } else {
            nc = na + nb - 1;
        }
        // System.out.println("na="+na);
        // System.out.println("nb="+nb);
        Integer ioffset = 0;
        Integer joffset = 0;

        Integer ZERO = 0;
        if (ma == ZERO || mb == ZERO || mc == ZERO || nc == ZERO) {
            return new BigDecimal[ZERO][ZERO];
        }

        for (int jc = 1; jc <= nc; jc++) {
            int j = jc + joffset;
            int jp1 = j + 1;
            int ja1 = 0;

            if (nb < jp1) {
                ja1 = jp1 - nb;
            } else {
                ja1 = 1;
            }

            int ja2 = 0;
            if (na < j) {
                ja2 = na;
            } else {
                ja2 = j;
            }

            for (int ic = 1; ic <= mc; ic++) {
                int i = ic + ioffset;
                int ip1 = i + 1;

                int ia1 = 0;
                if (mb < ip1) {
                    ia1 = ip1 - mb;
                } else {
                    ia1 = 1;
                }

                int ia2 = 0;
                if (ma < i) {
                    ia2 = ma;
                } else {
                    ia2 = i;
                }
                BigDecimal s = point(0);

                for (int ja = ja1; ja <= ja2; ja++) {

                    int jb = jp1 - ja;
                    for (int ia = ia1; ia <= ia2; ia++) {

                        int ib = ip1 - ia;
                        s = s.add(a[ia - 1][ja - 1].multiply(b[ib - 1][jb - 1]));
                    }
                }
                yList.add(s);
            }
        }
        y = new BigDecimal[1][yList.size()];
        for (int flag = 0; flag < yList.size(); flag++) {
            y[0][flag] = yList.get(flag);
        }

        if (x.length > 1) {
            y = transposeMatrix(y);
        }

        return y;
    }

    /**
     * reverse array only rows
     * <p>
     * <p>
     * function y = fliplr_eod(x) tam = size(x); %N�mero de filas y de columnas
     * filas = tam(1); %N�mero de filas columnas = tam(2); %N�mero de columnas
     * <p>
     * for i = 1:filas for j = 1:floor(columnas/2) %floor quita decimales temp =
     * x(i,j); x(i,j) = x(i,columnas-j+1); x(i,columnas-j+1) = temp; end end y =
     * x;
     *
     * @param matrix
     */
    public void fliplr_eod(BigDecimal[][] matrix) {
        BigDecimal[] row = matrix[0];
        List<BigDecimal> list = Arrays.asList(row);
        Collections.reverse(list);
        row = (BigDecimal[]) list.toArray();

    }

    public BigDecimal[][] linspace(Integer min, Integer max, Integer lenWAV) {
        BigDecimal[][] d = new BigDecimal[1][lenWAV];
        for (int i = 0; i < lenWAV; i++) {
            d[0][i] = point(min.doubleValue() + i
                    * (max.doubleValue() - min.doubleValue()) / (lenWAV - 1));
        }
        return d;
    }

    public BigDecimal[][] cumsum(BigDecimal[][] in) {
        BigDecimal[][] out = new BigDecimal[in.length][in[0].length];
        out[0][0] = in[0][0];
        for (int i = 1; i < out[0].length; i++) {
            out[0][i] = out[0][i - 1].add(in[0][i]);
        }

        return out;
    }

    /**
     * %RESULTADO: Se obtiene una senial ECG sin ruido
     *
     * @param signal
     * @param n
     * @return
     */
    public BigDecimal[][] medfil_eod(BigDecimal[][] signal, Integer n) {
        // printMatrix(signal);
        BigDecimal[][] signalTranspose = transposeMatrix(signal); // =x
        // printMatrix(signalTranspose);
        Integer blksz_length = signalTranspose.length;
        Integer blksz_width = signalTranspose[0].length;

        BigDecimal[][] y = new BigDecimal[blksz_length][blksz_width];
        fillMatrixWith(y, point(0));
        // printMatrix(y, "y");
        Integer nx = blksz_length;
        Integer m = 0;

        if (n % 2 == 0) {
            m = n / 2;
        } else {
            m = (n - 1) / 2;
        }

        BigDecimal[][] zeros = new BigDecimal[m][1];
        fillMatrixWith(zeros, point(0));
        signalTranspose = consolidate(zeros, signalTranspose, zeros);
        // printMatrix(signalTranspose, "ind");

        BigDecimal[][] indr = createColumn(n);// n=200

        // printMatrix(indr, "indr");
        BigDecimal[][] indc = createRow(1, nx);// nx=blksz=15000
        // printMatrix(indc, "indc");

        // BigDecimal[][] ones = null;
       
        BigDecimal[][] xx = null;
        
        int indrLength = indr.length;
        int indcLength = indc[0].length;
        BigDecimal[][] ind = new BigDecimal[indrLength][indcLength];
        ind = createTemplateMatrix(indr, indc, ind);
        int min = 0;
        zeros = null;
        for (int i = 1; i <= blksz_length; i = i + nx) {// VALIDAR SI ES MENOR O
            // IGUAL
            // ones = point[1][n];
            // fillMatrixWith(ones, point(1));
            // printMatrix(ones);

            // BigDecimal[][] row = createRow(i,nx);
            // printMatrix(row);
            // printMatrix(ind, "ind");
            min = blksz_length - i + 1;
            extrapolateMatrix(signalTranspose, ind);
            // System.out.println("====ind===");
            // printMatrix(ind);
            zeros = new BigDecimal[n][min];
            fillMatrixWith(zeros, point(0));
            xx = reshape_eod2(ind, zeros);
//            printMatrix(xx, "xx");

            // BigDecimal[][] createRow = createRow(i, nx);
            // printMatrix(createRow);

            calculateColumnMedian(y, xx);
            // printMatrix(y, "y");
        }
        
        BigDecimal[][] yTranspose = transposeMatrix(y);
        // printMatrix(yTranspose, "yTranspose");

        return yTranspose;
    }

    private void calculateColumnMedian(BigDecimal[][] y, BigDecimal[][] xx) {
        BigDecimal[][] xxTranspose = transposeMatrix(xx);

        for (int w = 0; w < xxTranspose.length; w++) {
            y[w][0] = median(xxTranspose[w]);
        }
        xx = null;
    }

    private BigDecimal median(BigDecimal[] numArray) {
        Arrays.sort(numArray);
        BigDecimal median = null;
        if (numArray.length % 2 == 0) {
            median = point((numArray[numArray.length / 2]
                    .add(numArray[numArray.length / 2 - 1])).divide(point(2))
                    .doubleValue());
        } else {
            median = numArray[numArray.length / 2];
        }
        return median;
    }

    private BigDecimal[][] reshape_eod2(BigDecimal[][] matrix, BigDecimal[][] zeros) {
   
    	
    	int proccesors = matrix.length/100;
    	ThreadEod t = null;
    	for(int i=0;i<proccesors;i++){
    		
    		
    		int start = 0+(100*i);
			int end = (i+1)*100;
			
			t = new ThreadEod(start, end, matrix, zeros);
    		t.run();
    		
    	}
    	
        return zeros;
    }

    private static BigDecimal[][] abs(BigDecimal[][] matrix) {
        BigDecimal[][] abs = new BigDecimal[matrix.length][matrix[0].length];
        for (int w = 0; w < matrix.length; w++) {
            for (int j = 0; j < matrix[0].length; j++) {
                abs[w][j] = matrix[w][j].abs();
            }
        }
        return abs;
    }


    private static BigDecimal[][] limitRow(BigDecimal[][] matrix, Integer from, Integer to) {

        BigDecimal[][] limit = new BigDecimal[1][to - from + 1];
        int x = 0;
        for (int j = from; j < to + 1; j++) {

            limit[0][x] = matrix[0][j - 1];
            x++;
        }

        return limit;
    }

    private static BigDecimal maxValuePosition(BigDecimal[][] matrix) {
        double max = 0;
        Integer maxValueColumn = null;
        for (int w = 0; w < matrix.length; w++) {
            for (int j = 0; j < matrix[0].length; j++) {
                if (matrix[w][j].doubleValue() > max) {
                    maxValueColumn = j;
                    max = matrix[w][j].doubleValue();
                }
            }
        }
        return point(maxValueColumn + 1);
    }

    private static void populateMatrixByIndexRowMatrix(BigDecimal[][] index,
                                                       BigDecimal[][] matrix) {
        for (int w = 0; w < index[0].length; w++) {
            index[0][w] = matrix[0][index[0][w].intValue() - 1];
        }
    }

    private void extrapolateMatrix(BigDecimal[][] signalTranspose,
                                   BigDecimal[][] ind) {
        int flag = 0;
        for (int w = 0; w < ind.length; w++) {
            for (int j = 0; j < ind[0].length; j++) {
                ind[w][j] = signalTranspose[j + flag][0];
            }
            flag++;

        }
    }

    private BigDecimal[][] createTemplateMatrix(BigDecimal[][] indr,
                                                BigDecimal[][] indc,
                                                BigDecimal[][] ind) {
        int indrLength = indr.length;
        int indcLength = indc[0].length;
        for (int w = 0; w < indrLength; w++) {
            for (int j = 0; j < indcLength; j++) {
                ind[w][j] = indr[w][0].add(indc[0][j]);
            }
        }
        return ind;

    }

    //crea columna y llena de valores consecutivos de 0...
    public BigDecimal[][] createColumn(int length) {
        BigDecimal[][] column = new BigDecimal[length][1];
        for (int i = 0; i < length; i++) {
            column[i][0] = point(i);
        }
        return column;
    }

    /**
     * i:min(i+blksz-1,nx)
     *
     * @param from
     * @param length
     * @return
     */
    public static BigDecimal[][] createRow(int from, int length) {
        BigDecimal[][] row = new BigDecimal[1][length - from + 1];
        for (int i = 0; i <= length; i++) {
            if (from <= length) {
                row[0][i] = point(from);
                from++;
            }
        }
        return row;
    }


    //X = [zeros(m,1); x; zeros(m,1)];
    public BigDecimal[][] consolidate(BigDecimal[][]... matrix) {

        int consolidateLenght = 0;
        int consolidateWidth = 1;
        for (int i = 0; i < matrix.length; i++) {
            consolidateLenght = consolidateLenght + matrix[i].length;
        }

        BigDecimal[][] consolidateMatrix = new BigDecimal[consolidateLenght][consolidateWidth];
        int adjustment = 0;
        for (int flag = 0; flag < matrix.length; flag++) {
            BigDecimal[][] tempMatrix = matrix[flag];

            for (int i = 0; i < tempMatrix.length; i++) {
                for (int j = 0; j < tempMatrix[i].length; j++) {
                    consolidateMatrix[i + adjustment][j] = tempMatrix[i][j];
                }
            }
            adjustment = adjustment + tempMatrix.length;
        }

        return consolidateMatrix;

    }

    //zeros ones en matlab
    public void fillMatrixWith(BigDecimal[][] matrix, BigDecimal value) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                matrix[i][j] = value;
            }
        }
    }

    /**
     * y = y' Transpuesta, transforma un vector fila en vector columna
     *
     * @param m
     * @return
     */
    public static BigDecimal[][] transposeMatrix(BigDecimal[][] m) {
        BigDecimal[][] temp = new BigDecimal[m[0].length][m.length];
        for (int i = 0; i < m.length; i++){
            for (int j = 0; j < m[0].length; j++){
                temp[j][i] = m[i][j];
            }
        }
        m = null;
        
        return temp;
    }

    public static void printMatrix(BigDecimal[][] matrix, String txt) {
//        System.out.println(sdf.format(new Date())+  "-wilder: ========== " + txt + " ==========="+matrix.length);
        
//        for (int i = 0; i < matrix.length; i++) {
//            System.out.println(new Date()+":Wilder: inicio print");
//            for (int j = 0; j < matrix[i].length; j++) {
               // Log.d("wilder:",""+j+":"+matrix[i][j] + "\t");
              //  System.out.println("" + j + ": " + matrix[i][j]);
//            }
//            System.out.println("Wilder: fin print ");
//        }

    }
    
    public static void printMatrix2(BigDecimal[][] matrix, String txt) {
      System.out.println(sdf.format(new Date())+  ": ========== " + txt + " ==========="+matrix.length);
      
      for (int i = 0; i < matrix.length; i++) {
//          System.out.println(new Date()+":Wilder: inicio print");
          for (int j = 0; j < matrix[i].length; j++) {
//              Log.d("wilder:",""+j+":"+matrix[i][j] + "\t");
              System.out.println("" + j + ": " + matrix[i][j]);
          }
//          System.out.println("Wilder: fin print ");
      }

  }
    

    //resta los valore de las matrices
    private BigDecimal[][] substract(BigDecimal[][] filsig1,
                                     BigDecimal[][] filsig3) {
        BigDecimal[][] D_ACOND = new BigDecimal[filsig1.length][filsig1[0].length];
        for (int i = 0; i < filsig1.length; i++) {
            for (int j = 0; j < filsig1[i].length; j++) {
                D_ACOND[i][j] = filsig1[i][j].subtract(filsig3[i][j]);
            }
        }
        return D_ACOND;
    }
//multiplica un numeroa todos los elementos de la matriz
    private void multiply(BigDecimal multiplier, BigDecimal[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = point(array[i][j].multiply(multiplier)
                        .doubleValue());
            }
        }
    }

    //suma un numeroa todos los elementos de la matriz
    private void add(BigDecimal adder, BigDecimal[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = point(array[i][j].add(adder).doubleValue());
            }
        }
    }

    //divide el numero con todos los numeros del array
    private void divide(BigDecimal divisor, BigDecimal[][] array, int precision) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = point(array[i][j].divide(divisor, precision,
                        RoundingMode.FLOOR).doubleValue());
            }
        }
    }

    public static BigDecimal point(String val) {
        BigDecimal x = new BigDecimal(val);
        return setScaleLuis(x);
    }

    public static BigDecimal point(double val) {
        BigDecimal x = new BigDecimal(val);
        return setScaleLuis(x);
    }

    private static BigDecimal setScaleLuis(BigDecimal x) {
        return x.setScale(DECIMAL_PRECISION, ROUNDING_MODE);
    }


    private static String[] wavalet;
    
    public static void ejecutar(BigDecimal[][] entrada, int cantidaDeValores, String[] wavaletE) {
        filsig1 = entrada;
        SAMPLES2READ = cantidaDeValores;
        wavalet = wavaletE;
        main(null);

    }

    public static void ejecutar(BigDecimal[][] entrada, int cantidaDeValores) {
        filsig1 = entrada;
        SAMPLES2READ = cantidaDeValores;
        main(null);

    }

    public static String[] readFile(String file){
    	StringBuilder text = new StringBuilder();

        dataList = new ArrayList<>();


        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;
            while ((line = br.readLine()) != null) {
                text.append(line);
            }
            br.close();
        } catch (IOException e) {
            System.err.println(e);
        }
        
//        System.out.println(text);
        
       return text.toString().split(SEPARATOR_FILE);
        
        
        
//        for (int i = 0; i < dataArray.length; i += 13) {
//            Double[] tmp = new Double[13];
//            for (int j = 0; j < 13; j++) {
//                tmp[j] = Double.parseDouble(dataArray[i + j]);
//            }
//            dataList.add(tmp);
//        }
    }
}
